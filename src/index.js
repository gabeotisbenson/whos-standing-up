import 'core-js/stable';
import 'regenerator-runtime/runtime';
import App from '~/App';
import store from '~/modules/store';
import Vue from 'vue';

const app = new Vue({ ...App, name: 'WhosStandingUp', store }).$mount('#app-root');

export default app;
