import { shuffle } from '@gabegabegabe/utils/dist/array/sorters';
import storage from '~/modules/storage';
import Vue from 'vue';
import Vuex, { Store } from 'vuex';

Vue.use(Vuex);

const TEAM_STORAGE_KEY = 'team-members';
const SET_TEAM_KEY = 'setTeam';
const LOAD_TEAM_KEY = 'loadTeam';

const store = new Store({
	state: {
		teamMembers: []
	},
	getters: {
		todaysList: state => state.teamMembers.sort(shuffle)
	},
	mutations: {
		[SET_TEAM_KEY]: (state, team) => {
			state.teamMembers = [...team];
		}
	},
	actions: {
		addMember: ({ commit }, member) => {
			const members = storage.get(TEAM_STORAGE_KEY) || [];
			members.push(member);
			storage.set(TEAM_STORAGE_KEY, members);
			commit(SET_TEAM_KEY, storage.get(TEAM_STORAGE_KEY));
		},
		removeMember: ({ commit }, member) => {
			const members = storage.get(TEAM_STORAGE_KEY) || [];

			const index = members.indexOf(member);

			if (index === -1) return;

			members.splice(index, 1);
			storage.set(TEAM_STORAGE_KEY, members);
			commit(SET_TEAM_KEY, storage.get(TEAM_STORAGE_KEY));
		},
		[LOAD_TEAM_KEY]: ({ commit }) => {
			commit(SET_TEAM_KEY, storage.get(TEAM_STORAGE_KEY) || []);
		}
	}
});

store.dispatch(LOAD_TEAM_KEY);

export default store;
