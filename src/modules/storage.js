const STORAGE_PREFIX = 'whos-standing-up-';

export default {
	set: (key, value) => window.localStorage.setItem(`${STORAGE_PREFIX}${key}`, JSON.stringify(value)),
	get: key => {
		try {
			const cachedValue = window.localStorage.getItem(`${STORAGE_PREFIX}${key}`);

			// eslint-disable-next-line no-undefined
			if (cachedValue !== null && cachedValue !== undefined) return JSON.parse(cachedValue);

			return null;
		} catch (err) {
			return null;
		}
	}
};
